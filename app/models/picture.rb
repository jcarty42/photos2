class Picture < ActiveRecord::Base

	has_attached_file :upload,
	log: true,
	storage: :s3,
	url: "s3-eu-west-1.amazonaws.com",	
	s3_credentials: "#{Rails.root}/config/aws.yml",
	s3_host_name: "s3-eu-west-1.amazonaws.com",
  path: ':class/:attachment/:id_:style_:filename',
  styles: lambda{|a| a.instance.image? ? {small: '200x200#', medium: '800x800>'} : {}},
  convert_options: { all: '-auto-orient' }
  #processors: [ :auto_orient, :thumbnail ] 

	belongs_to :album
	
  scope :images, ->{ where("upload_content_type LIKE 'image/%'") }
  scope :files, ->{ where("upload_content_type NOT LIKE 'image/%'") }
  scope :private, ->{ where(protected: true)}
  scope :public, ->{where(protected: false)}

  # Determine if the current file is an image or not
  def image?
    !!(upload_content_type.match(/^image/) && !upload_content_type.match(/svg/))
  end

  def content_type(picture)
    filtype = ''
    (picture.upload_content_type.match(/^image/) && !picture.upload_content_type.match(/svg/)) ? filetype = 'image' : filetype = 'file'

    return filetype
  end

end

