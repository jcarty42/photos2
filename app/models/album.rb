class Album < ActiveRecord::Base

	has_many :pictures, dependent: :destroy
	accepts_nested_attributes_for :pictures, allow_destroy: true
	validates_presence_of :name

  def self.find_updated_and_new(albums)
    album_array = [].to_set
    albums.each do |a|
      if a.pictures.length > 0 
        a.pictures.each do |p|
          if p.created_at.to_date == Date.yesterday and !p.protected
            album_array << a 
          end
        end
      end
    end
    album_array
  end

  def self.email_update
    albums = find_updated_and_new(Album.all)
    if albums.length > 0
      User.all.each do |u|
        PhotoAppMailer.email_updates_albums(u, albums).deliver
      end
    end
  end

end
