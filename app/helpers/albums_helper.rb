module AlbumsHelper

  def media_file_icon(content_type)
    klass = if content_type.match(/zip|rar|tar/)
      'compressed'
    elsif content_type.match(/video|flv-application|x-mp4/)
      'film'
    else
      'file'
    end

    content_tag(:i, nil, class: klass + ' file-icon glyphicon glyphicon-' + klass)
  end
  
  def is_protected(file)
    !file.protected ? klass = 'btn-default' : klass = 'btn-success'
    klass
  end

end
