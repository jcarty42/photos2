module ApplicationHelper

	def active_page(path)
		request.path == path ? 'active' : ''
	end

  def generate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    token
  end
end
