module PicturesHelper


  def authenticate_content(collection)
    if !current_user.admin
      authenticated_collection = []
      collection.each do |c|
        if !current_user.admin && c.protected == false
          authenticated_collection << c 
        end
      end
      
      collection = authenticated_collection
    end
    collection
  end

end

