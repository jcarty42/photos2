class PhotoAppMailer < ActionMailer::Base

  default from: "no-reply@photo-app.com"

  def confirm_account(user)
  	@user = user
  	mail(to: @user.email, subject: 'Confirm your account at the Carty Photo App')
  end

  def password_reset(user)
    @user = user
    mail(to: @user.email, subject: 'Reset password at the Carty Photo App')
  end

  def email_updates_albums(user, albums)
    @user = user
    @albums = albums
    mail(to: @user.email, subject: 'Album updates at the Carty Photo App')
  end

end
