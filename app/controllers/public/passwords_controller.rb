class Public::PasswordsController < ApplicationController

  skip_before_action :authenticate_user

  def new

  end

  def create
    session[:user_id] = nil
    @user = User.find_by_email(params[:email])
    if @user
      @user.validate_password = false
      @user.update(user_params)
      PhotoAppMailer.password_reset(@user).deliver
      flash[:notice] = 'An email has been sent with instuctions for a new password'
    else
      flash[:alert] = 'The email address does not have an account'
    end                              
    redirect_to new_user_session_path
  end

  def edit
    session[:user_id] = nil
    @user = User.find(params[:user])
  end

  def update
    session[:user_id] = nil
    user = User.find_by_reset_password_token(params[:reset_password_token])
    if user 
      @result = user.update(user_params)
      if @result
        session[:user_id] = user.id
        flash[:notice] = "Your password has been updated!"
        redirect_to root_url
      else
        session[:user_id] = nil
        errors =  user.errors.messages
        redirect_to :back, alert: errors
      end
    else
      redirect_to :back, alert: 'The email address does not match any account'
    end
  end

  def user_params
    params.permit(:id,:name, :email, :password, :admin, :password_confirmation, :confirmed_at, :reset_password_token, :reset_password_sent_at)
  end

end