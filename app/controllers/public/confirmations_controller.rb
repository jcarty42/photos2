class Public::ConfirmationsController < ApplicationController
  
  skip_before_action :authenticate_user

  def create
    session[:user_id] = nil
    @user = User.find_by_email(params[:email])

    if @user
      @user.validate_password = false
      @user.update(user_params)
      PhotoAppMailer.confirm_account(@user).deliver
      flash[:notice] = 'A new confirmation email has been sent'
    else
      flash[:alert] = 'This email address does not have an account'
    end
    redirect_to new_user_session_path
  end

  def update
    session[:user_id] = nil
    user = User.find_by(confirmation_token: params[:confirmation_token])
    if user.update(user_params)
      session[:user_id] = user.id
      flash[:notice] = "Your account has been confirmed!"
      redirect_to root_url
    else
      session[:user_id] = nil
      errors =  user.errors.messages
      redirect_to :back, alert: errors
    end
  end

  def edit
    session[:user_id] = nil
    @user = User.find(params[:user])
    if !@user.confirmed_at.blank?
      session[:user_id] = @user.id
      flash[:notice] = "Your account has already been confirmed!"
      redirect_to root_url
    elsif !@user.confirmation_sent_at.nil?
      if @user.confirmation_sent_at < (Time.now - 10.days)
        flash[:notice] = "The confirmation link is no longer valid"
        redirect_to new_confirmation_path
      end
    else
      flash[:notice] = "The confirmation link is not valid"
      redirect_to new_confirmation_path
    end
    @confirmation_token = params[:confirmation_token]
  end

  def user_params
    params.permit(:id,:name, :email, :password, :admin, :password_confirmation, :confirmed_at, :confirmation_token, :confirmation_sent_at)
  end

end