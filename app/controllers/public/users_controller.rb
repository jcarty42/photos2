class Public::UsersController < ApplicationController

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if !params[:user][:password].blank?
      @user.password_change = true
      if @user.authenticate_passwords(params)
        @result = @user.update(user_params)
      end
    else  
      @user.validate_password = false
      @result = @user.update(user_params)
    end
    if @result
      flash[:notice] = "Your account has been updated"
      redirect_to root_path
    else
      errors = @user.errors.messages
      redirect_to :back, alert: errors
    end
  end

  private

  def user_params
    params.require(:user).permit(:id,:name, :email, :password, :admin, :password_confirmation, :confirmed_at, :confirmation_token, :current_password)
  end

end