class Public::PicturesController < ApplicationController

  def index
    @pictures = Picture.all
    @picture = Picture.new
  end

  def new
    @picture = Picture.new
  end

 

  private

  def picture_params
    params.require(:picture).permit(
      :s3_url,
      :upload,
      :upload_file_name,
      :upload_content_type,
      :upload_file_path,
      :upload_file_size,
      :upload_updated_at,
      :upload_file_path,
      :name,
      :album_id
      )
  end

end
