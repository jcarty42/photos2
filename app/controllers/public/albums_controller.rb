class Public::AlbumsController < ApplicationController

  def index
    @albums = Album.all.order(:id).paginate(page: params[:page], per_page: 10)
    if params[:redirect]
      if params[:album_id]
        redirect_to album_path(params[:album_id])
      end   
    end
  end

  def show
    @album = Album.find(params[:id])
    @pictures = @album.pictures.paginate(page: params[:page], per_page: 20)
  end

  private

  def album_params
    params.require(:album).permit(:name, pictures_attributes: [:upload, :name, :album_id, :id, :protected])
  end

end
