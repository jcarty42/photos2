class Admin::PicturesController < ApplicationController

  before_filter :authenticate_admin

  def destroy
    @picture = Picture.find(params[:id])
    @picture.destroy
    respond_to do |f|
      f.html { redirect_to admin_albums_path }
      f.js
    end
  end

  def make_protected
    @picture = Picture.find(params[:id])
    @picture.protected ? @picture.protected = false : @picture.protected = true
    @picture.save
    respond_to do |f|
      f.html { redirect_to admin_albums_path }
      f.js
    end
  end

  def show
    @picture = Picture.find(params[:id])
  end

  private

  def picture_params
    params.require(:picture).permit(
      :s3_url,
      :upload,
      :upload_file_name,
      :upload_content_type,
      :upload_file_path,
      :upload_file_size,
      :upload_updated_at,
      :upload_file_path,
      :name,
      :album_id,
      :rotate
      )
  end

end
