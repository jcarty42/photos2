class Admin::UsersController < ApplicationController

  before_filter :authenticate_admin

  layout 'admin'

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.validate_password = false
    if @user.save
      redirect_to admin_users_path, :notice => "A confirmation notice has been sent to the user."
      PhotoAppMailer.confirm_account(@user).deliver
    else
      render "new"
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if !params[:user][:password].blank?
      @user.password_change = true
      if @user.authenticate_passwords(params)
        @result = @user.update(user_params)
      end
    else  
      @user.validate_password = false
      @result = @user.update(user_params)
    end
    if @result
      flash[:notice] = "Your account has been updated"
      redirect_to admin_root_path
    else
      errors = @user.errors.messages
      redirect_to :back, alert: errors
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
  end

  private

  def user_params
    params.require(:user).permit(:id,:name, :email, :password, :admin, :password_confirmation, :confirmed_at, :confirmation_token, :confirmation_sent_at, :current_password)
  end

end