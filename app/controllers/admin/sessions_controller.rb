class Admin::SessionsController < ApplicationController

	skip_before_action :authenticate_user

	def new
	end



	def destroy
	  session[:user_id] = nil
	  redirect_to new_session_path, :notice => "Logged out!"
	end
end