class Admin::AlbumsController < ApplicationController

  before_filter :authenticate_admin

  layout 'admin'

  def index
    @albums = Album.all.order(:id).paginate(page: params[:page], per_page: 10)
  end

  def new
    @album = Album.new
  end

  def create
    @album = Album.create(album_params)

    @result = @album.save
    respond_to do |f|
      f.js
      f.html {
        redirect_to action: :index
        flash[:notice] = 'Album succesfully created'
      }
    end
    
  end

  def edit
    @album = Album.find(params[:id])
    @picture = Picture.new
    @pictures = @album.pictures.paginate(page: params[:page], per_page: 20)
  end

  def update
    @album = Album.find(params[:id])
    @result = @album.update(album_params)
    if @result
      if params[:picture]
        params[:picture].each do |k,v|
          @save = @album.pictures.create(upload: v)
          @type = @save.content_type(@save)
        end
      end
    end
    @new_picture = @album.pictures.order('created_at').last
    respond_to do |f|
      f.js
      f.html {
        redirect_to action: :index
        flash[:notice] = 'Album succesfully updated'
      }
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @destroy = @album.destroy
    if @destroy
      redirect_to action: :index
      flash[:notice] = 'The album has been successfully deleted!'
    else
      redirect_to :back, alert: 'There was an error in deleting the album, please try again'
    end
  end

  private

  def album_params
    params.require(:album).permit(:name, :cover, :created_by, pictures_attributes: [:upload, :name, :album_id, :id])
  end

end
