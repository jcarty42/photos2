class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user

  before_action :authenticate_user

  private

  def authenticate_user
    if !current_user
      if params[:redirect]
        if params[:album_id]
          redirect_to new_user_session_url(album_id: params[:album_id])
        end
      else
        redirect_to new_user_session_path
      end
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authenticate_admin
    if current_user.admin != true
      redirect_to root_path
    end
  end

end
