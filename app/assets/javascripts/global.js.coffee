
window.App = {}

App.ready = ->

  if $('.album-pic-list').length > 0
    Code.photoSwipe "a", ".album-pic-list"

    Code.PhotoSwipe.Current.setOptions 
      enableMouseWheel: false
      enableKeyboard: false
      autoStartSlideshow: true
      zIndex: 2000

    Code.PhotoSwipe.Current.addEventListener Code.PhotoSwipe.EventTypes.onBeforeHide, (e) ->
      location.reload()
  

  multiple = false
  progress = 0

  # uploading photos
  $('#picture_upload').fileupload
    dataType: 'script'
    sequentialUploads: true
    add: (e, data) ->
      file = data.files[0]
      data.submit()
      $('.modal').modal('show')
    progressall:  (e, data) ->
      multiple = true
      progress = parseInt(data.loaded / data.total * 100, 10)
      $('.progress-bar').css('width', progress + '%')
      $('.percent').html(progress + '%')
    done: (e,data) ->
      if multiple
        if progress == 100
          $('.modal').modal('hide')
      else
        $('.modal').modal('hide')
      App.draggable()

  #album menu toggle
  $('.sidebar-toggle').on 'click', ->
    if $('.left-sidebar').hasClass('visible') 
      $('.left-sidebar').animate
        left: '-227'
        ,500
      $('.sidebar-responsive').animate
        left: '0'
        ,500  
      $('.left-sidebar').removeClass('visible') 
    else  
      $('.left-sidebar').animate
        left: '0'
        ,500
      $('.sidebar-responsive').animate
        left: '227'
        ,500  
      $('.left-sidebar').addClass('visible')    


  if $('#album-cover').length
    album_cover = true
  else
    album_cover = false

  # remove album cover
  $('.delete-cover span').on 'click', ->
    $('#album_cover').val('')
    $('.album-cover-edit img').remove()
    $('.delete-cover').stop()
    $('.delete-cover').fadeOut()
    album_cover = false

  # fade in and out album cover remove
  $('.album-cover-edit').on 'mouseenter', ->
    if album_cover == true
      $('.delete-cover').stop()
      $('.delete-cover').fadeIn()
  $('.album-cover-edit').on 'mouseleave', ->
    if album_cover == true
      $('.delete-cover').stop()
      $('.delete-cover').fadeOut()

  # Add new album sidebar
  $('.plus span').on 'click', ->
    App.hideShowAlbumForm()

  $('.album-delete-icon').on 'mouseover', ->
    $(this).tooltip('toggle')


# notices and alerts slide down from top of page
count = 0
message = ''
App.flash = (message, type) ->
  
  count++
  if message
    html = '<p>' + message + '</p>'
  if count > 1
    $('#' + type + ' .message').append(html)
  else
    $('#' + type + ' .message').append(html)
    $('#' + type ).slideToggle('fast')
    App.timeout(3000, 'set', type, message)

timer = ''
App.timeout = (time, option, type, message)->
  
  if option == 'set'
    timer = window.setTimeout ->
      $('#' + type ).slideToggle('fast')
      $('#' + type + ' .message').html('')
    ,time  
    
  if option == 'clear'
    
    window.clearTimeout(timer)

# change class of add new album icon
App.classToggle = (klass)->
  if klass == "glyphicon glyphicon-collapse-up pull-right"
    klass = "glyphicon glyphicon-plus-sign pull-right"
  else
    klass = "glyphicon glyphicon-collapse-up pull-right"
  return klass

App.hideShowAlbumForm  = ->
  $('.new-album-controlls form').slideToggle('fast')
  $('.plus span').attr('class', App.classToggle($('.plus span').attr('class')))

resize = ->
  if $(window).width() > 991
    $('.left-sidebar').css('left', '0')
  if $(window).width() < 991
    $('.left-sidebar').css('left', '-227px') 

# drag and drop
App.draggable = ->
  $( ".draggable" ).draggable
    opacity: 0.7,
    helper: "clone"
  $( ".droppable" ).droppable
    drop: (event, ui) ->
      html = ''
      src = ui.draggable.attr('src')
      src = src.replace("thumb","small")
      console.log src
      html += '<img alt="album cover" id="album-cover" class="album-cover-edit img-thumbnail" src="' + src + '">'

      $('.inner-box').prepend(html)
      $('#album_cover').val(src)
      album_cover = true 


$(document).ready ->
  App.ready() 
  App.draggable()
  App.timeout(null, 'clear', null, null) 

$(document).on 'page:load', ->
  App.draggable()
  App.ready()
  App.timeout(null, 'clear', null, null) 


$(window).resize ->
  resize()

