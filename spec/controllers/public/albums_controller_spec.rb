require "spec_helper"

describe Public::AlbumsController do
  
	let(:album) { FactoryGirl.create(:album) }
	let(:user) { FactoryGirl.create(:user) }

	describe 'GET #index' do
		it 'non logged in users get redirected to sign in' do
			get :index
			response.should redirect_to(new_user_session_path)
		end

		it 'logged in users can view index' do
			login(user)
			get :index
			response.should be_success
		end

		it 'if album_id in params, logged in users get redirected to album' do
			login(user)
			album
			get :index, album_id: album.id, redirect: 1
			response.should redirect_to(album_path(album))
		end
	end

	describe 'GET #show' do
		it 'non logged in users get redirected to sign in' do
			get :show, id: album
			response.should redirect_to(new_user_session_path)
		end

		it 'logged in users can view the album' do
			login(user)
			get :show, id: album
			response.should be_success
		end
	end
end
