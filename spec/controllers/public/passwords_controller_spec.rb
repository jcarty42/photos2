require "spec_helper"

describe Public::PasswordsController do

let(:user) { FactoryGirl.create(:user) }

  describe 'POST #create' do
    it 'sends new password email to user' do
      user
      post :create,  email: user.email, reset_password_token: generate_token
      email = ActionMailer::Base.deliveries.last
      email.subject.should == 'Reset password at the Carty Photo App'
      email.to.should == ["jason@jcartydesign.com"]
    end

    it 'creates a password reset token for user' do
      user
      post :create,  email: user.email, reset_password_token: generate_token
      user.reload.reset_password_token.should_not == nil
    end

    it 'redirects to sign in and does not create a password reset token if user not found' do
      post :create,  email: 'someone@email.com', reset_password_token: generate_token
      user.reload.reset_password_token.should == nil
      response.should redirect_to(new_user_session_path)
    end
  end

  describe 'GET #edit' do
    it 'finds user from params' do
      user
      get :edit, user: user.id
      response.should be_success
    end
  end

  describe 'POST #update' do
    it 'redirects to public root if all went well' do
      user
      user.save
      post :update, 
        id: user.id, 
        email: user.email, 
        password: 'password', 
        password_confirmation: 'password', 
        reset_password_token: user.reset_password_token
      response.should redirect_to(root_path)
    end

    it 'redirects back if the reset token is not right' do
      user
      @request.env['HTTP_REFERER'] = password_edit_url(user: user.id, reset_password_token: user.reset_password_token)
      user.save
      post :update, 
        id: user.id, 
        email: user.email, 
        password: 'password', 
        password_confirmation: 'password', 
        reset_password_token: generate_token
      response.should redirect_to(:back)
    end

    it 'redirects back if the email does not match user email' do
      user
      @request.env['HTTP_REFERER'] = password_edit_url(user: user.id, reset_password_token: user.reset_password_token)
      user.save
      post :update, 
        id: user.id, 
        email: 'someone@email.com', 
        password: 'password', 
        password_confirmation: 'password', 
        reset_password_token: generate_token
      response.should redirect_to(:back)
    end
  end
end