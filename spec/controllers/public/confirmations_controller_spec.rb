require "spec_helper"

describe Public::ConfirmationsController do

  let(:non_confirmed_user) { FactoryGirl.create(:user) }
  let(:admin) { FactoryGirl.create(:admin) }

   before :each do
    non_confirmed_user.validate_password = false
    non_confirmed_user.confirmation_token = generate_token
    non_confirmed_user.confirmed_at = nil
    non_confirmed_user.save
  end

  describe "POST #create" do

    it 'sends a confirmation email to new user' do
      post :create, email: non_confirmed_user.email
      email = ActionMailer::Base.deliveries.last
      email.subject.should == 'Confirm your account at the Carty Photo App'
      email.to.should == ["jason@jcartydesign.com"]
    end
  end

  describe "POST #update" do

    it 'redirects back if email is not filled in' do
      non_confirmed_user.validate_password = true
      @request.env['HTTP_REFERER'] = confirmations_edit_url(
                                    user: non_confirmed_user,
                                    confirmation_token: non_confirmed_user.confirmation_token
                                    )
      post :update, id: non_confirmed_user,
        email: '',
        password: 'password',
        password_confirmation: 'password',
        id: non_confirmed_user,
        confirmation_token: non_confirmed_user.confirmation_token,
        confirmed_at: Time.now
      response.should redirect_to(:back)
    end

    it 'redirects back if password is not filled in' do
      non_confirmed_user.validate_password = true
      @request.env['HTTP_REFERER'] = confirmations_edit_url(
                                    user: non_confirmed_user,
                                    confirmation_token: non_confirmed_user.confirmation_token
                                    )
      post :update, id: non_confirmed_user,
        email: non_confirmed_user.email,
        password_confirmation: 'password',
        id: non_confirmed_user,
        confirmation_token: non_confirmed_user.confirmation_token,
        confirmed_at: Time.now
      response.should redirect_to(:back)
    end

    it 'redirects back if password confirmation is not filled in' do
      non_confirmed_user.validate_password = true
      @request.env['HTTP_REFERER'] = confirmations_edit_url(
                                    user: non_confirmed_user,
                                    confirmation_token: non_confirmed_user.confirmation_token
                                    )
      post :update, id: non_confirmed_user,
        email: non_confirmed_user.email,
        password: 'password',
        password_confirmation: '',
        id: non_confirmed_user,
        confirmation_token: non_confirmed_user.confirmation_token,
        confirmed_at: Time.now
      response.should redirect_to(:back)
    end

    it 'redirects to root if everything is filled in and correct' do
      non_confirmed_user.validate_password = true
      @request.env['HTTP_REFERER'] = confirmations_edit_url(
                                    user: non_confirmed_user,
                                    confirmation_token: non_confirmed_user.confirmation_token
                                    )
      post :update, id: non_confirmed_user,
        email: non_confirmed_user.email,
        password: 'password',
        password_confirmation: 'password',
        id: non_confirmed_user,
        confirmation_token: non_confirmed_user.confirmation_token,
        confirmed_at: Time.now
      response.should redirect_to(root_path)
    end
  end

  describe "GET #edit" do
    it 'redirects to root if account is already confirmed' do
      admin.confirmed_at = Time.now
      admin.save
      get :edit, user: admin
      response.should redirect_to(root_path)
    end

    it 'lets the non confirmed user confirm account' do
      non_confirmed_user.confirmation_sent_at = Time.now - 9.days
      non_confirmed_user.save
      get :edit, user: non_confirmed_user
      response.should be_success
    end

    it 'redirects to new confirmation path if link is too old' do
      non_confirmed_user.confirmation_sent_at = Time.now - 11.days
      non_confirmed_user.save
      get :edit, user: non_confirmed_user
      response.should redirect_to(new_confirmation_path)
    end

    it 'redirects to new confirmation path if confirmation not sent' do
      get :edit, user: non_confirmed_user
      response.should redirect_to(new_confirmation_path)
    end

  end

end
