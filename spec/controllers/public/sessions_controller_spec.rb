require "spec_helper"

describe Public::SessionsController do
  
  let(:user) { FactoryGirl.create(:user) }

  describe '#POST create' do
    it 'does not create new session if password is invalid' do
      user
      post :create, email: user.email, password: 'something'
      response.should redirect_to(new_user_session_path)
    end 

    it 'does not create new session if email is invalid' do
      user
      post :create, email: 'test@test.com' , password: user.password
      response.should redirect_to(new_user_session_path)
    end 

    it 'creates new session if password and email is valid' do
      user
      post :create, email: user.email, password: user.password
      session[:user_id].should == user.id
      response.should redirect_to(root_path)
    end  
  end

  describe 'DELETE #destroy' do
    it 'allows logged in users to log out' do
      login(user)
      delete :destroy, id: user
      response.should redirect_to(new_user_session_path)
    end
  end

end
