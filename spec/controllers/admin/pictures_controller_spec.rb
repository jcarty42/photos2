require "spec_helper"

describe Admin::PicturesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:admin) { FactoryGirl.create(:admin) }
  let(:picture) { FactoryGirl.create(:picture) }

  describe 'DELETE #destroy' do

    it 'redirects non logged in users to sign in' do
      delete :destroy, id: picture
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      picture
      delete :destroy, id: picture
      response.should redirect_to(root_path)
    end
    it 'admin can destroy pictures' do
      login(admin)
      picture
      count = Picture.all.count
      delete :destroy, id: picture
      response.should redirect_to(admin_albums_path)
      Picture.all.reload.count.should == count - 1
    end
  end

  describe 'GET #make_peotected' do
    it 'redirects non logged in users to sign in' do
      get :make_protected, id: picture
      response.should redirect_to(new_user_session_path)
    end

    it 'redirect logged in users to public root' do
      login(user)
      get :make_protected, id: picture
      response.should redirect_to(root_path)
    end

    it 'admin can make pictures protected' do
      login(admin)
      picture
      get :make_protected, id: picture
      picture.reload
      picture.protected.should == true
    end
  end

end