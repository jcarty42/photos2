require "spec_helper"

describe Admin::SessionsController do
  
  let(:admin) { FactoryGirl.create(:admin) }

  describe 'DELETE #destroy' do
    it 'logs out admin and redirects to login' do
      login(admin)
      delete :destroy, id: admin
      response.should redirect_to(new_session_path)
    end
  end

end
