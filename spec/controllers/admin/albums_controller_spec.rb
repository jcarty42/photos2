require "spec_helper"

describe Admin::AlbumsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:admin) { FactoryGirl.create(:admin) }
  let(:album) { FactoryGirl.create(:album) }

  # before :each do
  #   @file = fixture_file_upload('test.png', 'image/png')
  # end

  describe 'GET #index' do
    it 'redirects not logged in users to sign in' do
       get :index
       response.should redirect_to(new_user_session_path) 
    end

    it 'redirects user to public root' do
      login(user)
      get :index
      response.should redirect_to(root_path)
    end

    it 'admin gets a 200 response' do
      login(admin)
      get :index
      response.should be_success
    end
  end

  describe 'GET #new' do
    it 'redirects not logged in users to sign in' do
      get :new
      response.should redirect_to(new_user_session_path) 
    end

    it 'redirects user to public root' do
      login(user)
      get :new
      response.should redirect_to(root_path)
    end

    it 'admin gets a 200 response' do
      login(admin)
      get :new
      response.should be_success
    end
  end

  describe 'GET #edit' do
    it 'redirects not logged in users to sign in' do
      get :edit, id: album
      response.should redirect_to(new_user_session_path) 
    end

    it 'redirects user to public root' do
      login(user)
      get :edit, id: album
      response.should redirect_to(root_path)
    end

    it 'admin gets a 200 response' do
      login(admin)
      get :edit, id: album
      response.should be_success
    end
  end

  describe 'POST #create' do
    it 'redirects not logged in users to sign in' do
      post :create, album: { name: 'test1'}
      response.should redirect_to(new_user_session_path) 
    end

    it 'redirects user to public root' do
      login(user)
      post :create, album: { name: 'test1'}
      response.should redirect_to(root_path)
    end

    it 'admin can create new album' do
      login(admin)
      count = Album.all.count
      post :create, album: { name: 'test1'}
      response.should redirect_to(admin_albums_path)
      Album.all.count.should == count + 1
    end


    # it 'admin can upload new pictures to album' do
    #   login(admin)
    #   count = Picture.all.count
    #   post :create, album: { name: 'Test2', pictures_attributes:[ upload: @file ] }
    #   Picture.all.reload.count.should == count + 1
    #   Picture.last.destroy
    # end
  
  end

  describe 'PATCH #update' do
    it 'redirects not logged in users to sign in' do
      patch :update, id: album, album: { name: 'test23'}
      response.should redirect_to(new_user_session_path) 
    end

    it 'redirects non admin user to public root' do
      login(user)
      patch :update, id: album, album: { name: 'test23'}
      response.should redirect_to(root_path)
    end

    it 'admin gets a 200 response' do
      login(admin)
      patch :update, id: album, album: { name: 'test23'}
      response.should redirect_to(admin_albums_path)
      album.reload.name.should == 'test23'
    end
  end

  describe 'DELETE #destroy' do
    it 'admin can destroy albums' do
      login(admin)
      album
      count = Album.all.count
      delete :destroy, id: album
      response.should redirect_to(admin_albums_path)
      Album.all.reload.count.should == count - 1
    end
  end

end
