require "spec_helper"

describe Admin::UsersController do

  let(:admin) { FactoryGirl.create(:admin) }
  let(:user) { FactoryGirl.create(:user) }

  describe 'GET #index' do
    it 'redirect non logged in users to log in' do
      get :index
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      get :index
      response.should redirect_to(:root)
    end

    it 'admin gets to see the user index page' do
      login(admin)
      get :index
      response.should be_success
    end
  end

  describe 'GET #new' do
    it 'redirects non logged in users to log in' do
      get :new
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      get :new
      response.should redirect_to(:root)
    end

    it 'admin gets to see the new user page' do
      login(admin)
      get :new
      response.should be_success
    end
  end

  describe 'POST #create' do 
    it 'redirects non logged in users to log in' do
      post :create, user: { name: 'Test Testsson', email: 'test@test.com', admin: false }
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      post :create, user: { name: 'Test Testsson', email: 'test@test.com', admin: false }
      response.should redirect_to(:root)
    end

    it 'admin can create a new user' do
      login(admin)
      count = User.all.count
      post :create, user: { name: 'Test Testsson', email: 'test@test.com', admin: false }
      response.should redirect_to(admin_users_url)
      User.all.reload.count.should == count + 1
    end

    it 'sends a confirmation email to user after creation' do
      login(admin)
      post :create, user: { name: 'Test Testsson', email: 'test@test.com', admin: false }
      email = ActionMailer::Base.deliveries.last
      email.subject.should == 'Confirm your account at the Carty Photo App'
      email.to.should == ['test@test.com']
    end
  end

  describe 'GET #edit' do
    it 'redirects non logged in users to log in' do
      get :edit, id: user
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      get :edit, id: user
      response.should redirect_to(:root)
    end

    it 'admin gets to see the edit user page' do
      login(admin)
      get :edit, id: user
      response.should be_success
    end
  end

  describe 'POST #update' do 
    it 'redirects non logged in users to log in' do
      post :update, id: user, user: { name: 'Test Johnsson'}
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      post :update, id: user, user: { name: 'Test Johnsson'}
      response.should redirect_to(:root)
    end

    it 'admin can update user' do
      login(admin)
      post :update, id: user, user: { name: 'Test Johnsson'}
      response.should redirect_to(admin_root_url)
      user.reload.name.should == 'Test Johnsson'
    end
  end

  describe 'DELETE #destroy' do
    it 'redirects non logged in users to log in' do
      xhr :delete, :destroy, id: user
      response.should redirect_to(new_user_session_path)
    end

    it 'redirects non admin to public root' do
      login(user)
      xhr :delete, :destroy, id: user
      response.should redirect_to(:root)
    end

    it 'admin gets can delete a user' do
      login(admin)
      user
      count = User.all.count
      xhr :delete, :destroy, id: user
      User.all.reload.count.should == count - 1
    end
  end

  
end
