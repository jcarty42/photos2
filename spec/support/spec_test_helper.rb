module SpecTestHelper   
  
  def login_admin
    login(:admin)	
  end

  def login(user)
    user = User.where(id: user.to_s).first if user.is_a?(Symbol)
    session[:user_id] = user.id  
  end

  def current_user
    User.find(request.session[:user_id])
  end

  def generate_token
    token = Digest::SHA1.hexdigest([Time.now, rand].join)
    token
  end
end
