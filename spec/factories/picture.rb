FactoryGirl.define do
	factory :picture do
		s3_url 'http://s3-eu-west-1.amazonaws.com/photo-app/pictures/uploads/115/original/IMG_2215.JPG?1401482317'
		upload_file_name 'IMG_2036.JPG'
		upload_content_type 'image/jpeg'
		upload_file_size '3375669'
		upload_updated_at '2014-05-19 18:56:30'
		upload_file_path '2014-05-19 18:56:30'
	end
end