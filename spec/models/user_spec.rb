require 'spec_helper'

describe User do

  let(:user) { FactoryGirl.create(:user) }

  describe 'User #find_by_login' do
    it 'finds a user if all login attributes are correct' do
      user
      @user, errors = User.find_by_login(email: 'jason@jcartydesign.com', password: 'password')
      errors.should == ""
      @user.should == user
    end

    it 'returns invalid email address if it does not match' do
      user
      @user, errors = User.find_by_login(email: 'jason@test.se', password: 'password')
      errors.should == "Invalid email address"
    end

    it 'returns invalid password if it does not match' do
      user
      @user, errors = User.find_by_login(email: 'jason@jcartydesign.com', password: 'incorrect_password')
      errors.should == "Invalid password"
    end

    it 'returns unconfirmed account if account is not confirmed' do
      user.confirmed_at = nil
      user.save
      @user, errors = User.find_by_login(email: 'jason@jcartydesign.com', password: 'password')
      errors.should == "This account has not yet been confirmed"
    end
  end

  describe 'User #confirmed?' do
    it 'checks if the user account has been confirmed' do
      result = user.confirmed?
      result.should == true
      user.confirmed_at = nil
      user.save
      non_confirmed_result = user.confirmed?
      non_confirmed_result.should == false
    end
  end

  describe 'User #authenticate_passwords' do
    it 'checks if current password is a match for inputed password' do
      result = user.authenticate_passwords({user:{current_password: 'password'}})
      result.should == true
    end
  end

  
end
