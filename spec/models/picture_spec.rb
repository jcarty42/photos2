require 'spec_helper'

describe Picture do

  let(:album) { FactoryGirl.create(:album) }

  describe 'Picture scopes' do
    before(:each) do
      album
      @file1 = album.pictures.create(
        upload_file_name: 'IMG_2036.css',
        upload_content_type: 'text/css',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30'
      ) 
      @file2 = album.pictures.create(
        upload_file_name: 'IMG_2036.pdf',
        upload_content_type: 'application/pdf',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30'
      ) 
      @picture1 = album.pictures.create(
        upload_file_name: 'IMG_2036.jpg',
        upload_content_type: 'image/jpeg',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30'
      ) 
      @picture2 = album.pictures.create(
        upload_file_name: 'IMG_2036.png',
        upload_content_type: 'image/png',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30'
      )
      @private_picture1 = album.pictures.create(
        upload_file_name: 'IMG_2036.jpg',
        upload_content_type: 'image/jpeg',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30',
        protected: true
      ) 
      @private_picture2 = album.pictures.create(
        upload_file_name: 'IMG_2036.png',
        upload_content_type: 'image/png',
        upload_file_size: '3375669',
        upload_updated_at: '2014-05-19 18:56:30',
        protected: true
      )

      @files = [@file1, @file2] 
      @images = [@picture1, @picture2, @private_picture1, @private_picture2]
      @private = [@private_picture1, @private_picture2]
    end

    it 'returns files that are only images' do
      expect(album.pictures.images).to match_array(@images)
    end

    it 'returns files that are not images' do
      expect(album.pictures.files).to match_array(@files)
    end

    it 'returns files that are only images' do
      expect(album.pictures.private).to match_array(@private)
    end

  end 

  describe 'Picture #image?' do
    it 'returns false if not image' do
      pdf = Picture.new(upload_content_type: 'application/pdf')
      pdf.image?.should be_false
      png = Picture.new(upload_content_type: 'image/png')
      png.image?.should be_true
    end
  end

  describe 'Picture "content_type' do
    it 'returns the content type of file' do
      pdf = Picture.new(upload_content_type: 'application/pdf')
      pdf.content_type(pdf).should == 'file'
      png = Picture.new(upload_content_type: 'image/png')
      png.content_type(png).should == 'image'
    end
  end
end