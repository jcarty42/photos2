require 'spec_helper'

describe Album do

  let(:user) { FactoryGirl.create(:user) }
  let(:admin) { FactoryGirl.create(:admin) }
  let(:album) { FactoryGirl.create(:album) }
  let(:photo) { FactoryGirl.create(:picture) }

  describe 'email_update' do

    it 'sends an email to each user if an album has been updated or new photos added' do
      photo.album_id = album.id
      photo.created_at = Date.yesterday
      photo.save
      user
      admin
      Album.email_update
      @count = 0
      ActionMailer::Base.deliveries.each { |n| @count = @count + 1 unless n.subject != 'Album updates at the Carty Photo App' }
      @count.should == User.all.count
      puts @count
    end

  end

end