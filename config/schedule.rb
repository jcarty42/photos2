
#job_type :rbenv_runner, %Q{PATH="$HOME/.rbenv/bin:$PATH"; eval "$(rbenv init -)"; cd :path && bundle exec rails runner -e :environment ':task' :output}

env :PATH, ENV['PATH']

every 1.days, at: '4:30 am'  do
  runner "Album.email_update"
end

# every 5.minutes  do
#   runner "Album.email_update"
# end

