Rails.application.routes.draw do
    
  scope module: 'public' do
    
    resources :pictures
    resources :albums do
      resource :pictures
    end

    root 'albums#index'

    delete "log_out", to: "sessions#destroy", as: "destroy_user_session"
    get "log_in", to: "sessions#new", as: "new_user_session"
    post "confirm_account_create", to: "confirmations#create", as: "confirmations"
    get "confirm_account_edit", to: "confirmations#edit", as: "confirmations_edit"
    post "confirm_account_update", to: "confirmations#update", as: "confirmations_update"


    get 'password_new', to: "passwords#new", as: 'password_new'
    post 'password_create', to: "passwords#create", as: 'password_create'
    get 'password_edit', to: "passwords#edit", as: 'password_edit'
    post 'password_update', to: 'passwords#update', as: 'password_update'

    resources :users
    resources :sessions
    resources :confirmations
    resources :passwords
  end
  

  namespace :admin do
    
    resources :pictures
    resources :albums do
      resource :pictures
    end

    root 'albums#index'

    delete "log_out", to: "sessions#destroy", as: "destroy_user_session"
    get "pictures/:id/make_protected", to: "pictures#make_protected", as: "pictures_make_protected"
    get "sign_up", to: "users#new", as: "new_user_registration"
    


    resources :users
    resources :sessions
    #resources :confirmations

  end
  
end
