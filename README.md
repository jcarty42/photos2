## Photos

This application was made so that my family and I would have a secure place to store and share our photos.

Ruby version 2.1.1

Rails version 4.1.0.beta1

### Development

To get started clone the repository and run the following in the terminal

```bash
git clone https://jcarty42@bitbucket.org/jcarty42/photos2.git && cd photos2
bundle
# If you are using a Mac with yosemite and you run into issues with Nokogiri and pg, you might need to run:
# ARCHFLAGS="-arch x86_64" bundle
```

Install ImageMagick
```bash
brew install imagemagick
brew install libpng # This step might not be neccesary
brew link libpng # This step might not be neccesary
```

Setup the databse
```bash
bundle exec rake db:create db:migrate db:seed
```

Start the server
```bash
rails s
```
Go to localhost:3000 in your favorite browser

An admin user was created from the db seed, checkout db/seeds.rb for the credentials for logging in.

#### Mailer

In the development environment the emails are sent through mailcatcher. To set this up, run:
```bash
gem install mailcatcher 
mailcatcher 
```
You will find all incoming emails at localhost:1080

#### Fileuploads
I have been using Amazon to store the uploaded files for this application. I am sure you understand that I will not be giving you the credentials for that. If you have your own amazon account then you should create a file called secrets.yml and place it in the config folder. It should look something like this:

```ruby

# secrets.yml

development:
  secret_key_base: 0981734509mcnnpncslzkjhncilhflicochqfw
  s3_key: your_s3_access_key_here
  s3_secret_key: you_s3_secret_key_here

test:
  secret_key_base: 0981734509mcnnpncslzkjhncilhflicochqfw
  s3_key: your_s3_access_key_here
  s3_secret_key: you_s3_secret_key_here

production:
  secret_key_base: 0981734509mcnnpncslzkjhncilhflicochqfw
  s3_key: your_s3_access_key_here
  s3_secret_key: you_s3_secret_key_here
  
```

## Production

#### Mailer

You will need to add an email account for the emails to be sent properly. Look in config/environments/production.rb to see what information needs to be added to config/secrets.yml

I have setup deployment with mina. After adding the correct information to config/deploy.rb (and setting up you server :)), run:
```bash
mina deploy
``` 

## Tests

Tests have been made with rspec. To run the tests run:
```bash
bundle exec rake
```
