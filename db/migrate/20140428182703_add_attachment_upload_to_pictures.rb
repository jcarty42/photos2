class AddAttachmentUploadToPictures < ActiveRecord::Migration
  def self.up
    change_table :pictures do |t|
      t.attachment :upload
    end
  end

  def self.down
    drop_attached_file :pictures, :upload
  end
end
