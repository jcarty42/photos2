class AddRotationToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :rotate, :integer
  end
end
