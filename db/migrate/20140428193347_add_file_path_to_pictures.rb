class AddFilePathToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :upload_file_path, :string
  end
end
