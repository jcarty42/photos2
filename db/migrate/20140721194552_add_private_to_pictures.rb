class AddPrivateToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :protected, :boolean, default: false
  end
end
