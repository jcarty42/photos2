class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :created_by
      t.string :name

      t.timestamps
    end
  end
end
