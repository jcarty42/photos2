class AddUniqueIndexToUsersForRememberMeAndResetPassword < ActiveRecord::Migration
  def change
    add_index "users", ["remember_me_token"], name: "index_users_on_remember_me_token", unique: true, using: :btree
  end
end
