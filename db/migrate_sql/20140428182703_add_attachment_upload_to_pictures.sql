ALTER TABLE "pictures" ADD COLUMN "upload_file_name" character varying(255);
ALTER TABLE "pictures" ADD COLUMN "upload_content_type" character varying(255);
ALTER TABLE "pictures" ADD COLUMN "upload_file_size" integer;
ALTER TABLE "pictures" ADD COLUMN "upload_updated_at" timestamp;
INSERT INTO schema_migrations (version) VALUES (20140428182703);
