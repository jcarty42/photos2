ALTER TABLE "users" ADD COLUMN "password_hash" character varying(255);
ALTER TABLE "users" ADD COLUMN "password_salt" character varying(255);
INSERT INTO schema_migrations (version) VALUES (20140511180007);
