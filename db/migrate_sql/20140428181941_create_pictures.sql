CREATE TABLE "pictures" ("id" serial primary key, "name" character varying(255), "s3_url" character varying(255), "album_id" integer, "created_at" timestamp, "updated_at" timestamp) ;
CREATE  INDEX  "index_pictures_on_album_id" ON "pictures"  ("album_id");
INSERT INTO schema_migrations (version) VALUES (20140428181941);
